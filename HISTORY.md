#### agw - a pyautogui wrapper


##### v0.3.0

- added agw cli utility commands:
    - cursor command for tracking cursor position.
    - jiggler command for keeping screen alive.
    - screen command for taking desktop images and find images on desktop.

##### v0.2.0

- added AutomatorApp cli
- added MacroDataFileHandler for common CSV text file handling.
- added datafile decorator for ease of use
